﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json;

namespace Otus.HW2
{
    class Program
    {
        private const int Iterations = 10000;

        static void Main(string[] args)
        {

            var obj = new List<F> { new F(1, 2, 3, 4, 5) };
            Console.WriteLine($"количество замеров: {Iterations} итераций");
            Console.WriteLine("мой рефлекшен: ");

            ConvertToCsvFile(obj);
            CsvToClass();

            Console.WriteLine("Cтандартный механизм (Cinchoo ETL): ");

            ConvertToCsvFileStandard(obj);
            CsvToClassStandard();

            Console.ReadLine();
        }

        private static void ConvertToCsvFile(IList<F> obj)
        {
            Stopwatch stopWatch = new();
            var cw = new CsvWriter<F>();

            stopWatch.Start();

            for (var i = 0; i < Iterations; i++)
            {
                cw.WriteFromEnumerable(obj, $"example{i}.csv");
            }
            stopWatch.Stop();

            var ts = stopWatch.Elapsed;
            Console.WriteLine($"Время на сериализацию: {ts.Milliseconds } мс.");
        }

        private static void CsvToClass()
        {
            Stopwatch stopWatch = new();
            var cr = new CsvReader<F>();
            stopWatch.Start();
            for (var i = 0; i < Iterations; i++)
            {
                cr.Read($"example{i}.csv", true);
            }
            stopWatch.Stop();
            var ts = stopWatch.Elapsed;

            Console.WriteLine($"Время на десериализацию : {ts.Milliseconds } мс.");
        }

        private static void ConvertToCsvFileStandard(IList<F> obj)
        {
            Stopwatch stopWatch = new();
            var cw = new CsvWriter<F>();

            stopWatch.Start();

            for (var i = 0; i < Iterations; i++)
            {
                cw.WriteFromEnumerableWithCinchoo(obj, $"example{i}.csv");
            }
            stopWatch.Stop();

            var ts = stopWatch.Elapsed;
            Console.WriteLine($"Время на сериализацию: {ts.Milliseconds } мс.");
        }

        private static void CsvToClassStandard()
        {
            Stopwatch stopWatch = new();
            var cr = new CsvReader<F>();
            stopWatch.Start();
            for (var i = 0; i < Iterations; i++)
            {
                cr.ReadWithCsvReader($"example{i}.csv");
            }
            stopWatch.Stop();
            var ts = stopWatch.Elapsed;

            Console.WriteLine($"Время на десериализацию : {ts.Milliseconds } мс.");
        }
    }

    internal class F : CsvableBase
    {
        public F()
        {

        }
        public F(int i1, int i2, int i3, int i4, int i5)
        {
            I1 = i1;
            I2 = i2;
            I3 = i3;
            I4 = i4;
            I5 = i5;
        }
        public int I1 { get; set; }
        public int I2 { get; set; }
        public int I3 { get; set; }
        public int I4 { get; set; }
        public int I5 { get; set; }
    }
}
