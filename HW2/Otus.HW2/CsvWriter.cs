﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.HW2
{
	public class CsvWriter<T> where T : CsvableBase
    {
        public void WriteFromEnumerable(IEnumerable<T> objects, string destination)
        {
            var objs = objects as IList<T> ?? objects.ToList();
            if (!objs.Any()) return;
            using var sw = new StreamWriter(destination);
            foreach (var obj in objs)
            {
                sw.WriteLine(obj.ToCsv());
            }
        }

        public void WriteFromEnumerableWithCinchoo(IEnumerable<T> objects, string destination)
        {
            var objs = objects as IList<T> ?? objects.ToList();
            if (!objs.Any()) return;
            using var sw = new StreamWriter(destination);
            foreach (var obj in objs)
            {
                sw.WriteLine(obj.ChoJSONReaderToCSV());
            }
        }

        public void WriteFromEnumerable(IEnumerable<T> objects, string destination,
            string[] propertyNames, bool isIgnore)
        {
            var objs = objects as IList<T> ?? objects.ToList();
            if (!objs.Any()) return;
            using var sw = new StreamWriter(destination);
            foreach (var obj in objs)
            {
                sw.WriteLine(obj.ToCsv(propertyNames, isIgnore));
            }
        }

        public void WriteFromEnumerable(IEnumerable<T> objects, string destination,
            int[] propertyIndexes, bool isIgnore)
        {
            var objs = objects as IList<T> ?? objects.ToList();
            if (!objs.Any()) return;
            using var sw = new StreamWriter(destination);
            foreach (var obj in objs)
            {
                sw.WriteLine(obj.ToCsv(propertyIndexes, isIgnore));
            }
        }
    }
}
