﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using ChoETL;
using Newtonsoft.Json;

namespace Otus.HW2
{
    public abstract class CsvableBase
    {
        public virtual string ToCsv()
        {
            var output = "";

            var properties = GetType().GetProperties();

            for (var i = 0; i < properties.Length; i++)
            {

                output += GetPropertyValue(properties[i]);

                if (i != properties.Length - 1)
                {
                    output += ",";
                }
            }

            return output;
        }

        public virtual string ChoJSONReaderToCSV()
        {
            var csv = new StringBuilder();
            var json = JsonConvert.SerializeObject(this);

            using var r = ChoJSONReader.LoadText(json);
            using var w = new ChoCSVWriter(csv).WithFirstLineHeader(true).UseNestedKeyFormat(true).ThrowAndStopOnMissingField(false);

            w.Write(r);

            return csv.ToString();
        }


        public virtual string ToCsv(string[] propertyNames, bool isIgnore)
        {
            var output = "";
            var isFirstPropertyWritten = false;


            var properties = GetType().GetProperties();

            foreach (var property in properties)
            {
                if (isIgnore)
                {
                    if (propertyNames.Contains(property.Name)) continue;

                    if (isFirstPropertyWritten)
                    {
                        output += ",";
                    }

                    output += GetPropertyValue(property);

                    if (!isFirstPropertyWritten)
                    {
                        isFirstPropertyWritten = true;
                    }
                }
                else
                {
                    if (!propertyNames.Contains(property.Name)) continue;

                    if (isFirstPropertyWritten)
                    {
                        output += ",";
                    }

                    output += GetPropertyValue(property);

                    if (!isFirstPropertyWritten)
                    {
                        isFirstPropertyWritten = true;
                    }
                }
            }

            return output;
        }

        public virtual string ToCsv(int[] propertyIndexes, bool isIgnore)
        {
            var output = "";

            var isFirstPropertyWritten = false;

            var properties = GetType().GetProperties();

            for (var i = 0; i < properties.Length; i++)
            {
                if (isIgnore)
                {
                    if (propertyIndexes.Contains(i)) continue;

                    if (isFirstPropertyWritten)
                    {
                        output += ",";
                    }

                    output += GetPropertyValue(properties[i]);

                    if (!isFirstPropertyWritten)
                    {
                        isFirstPropertyWritten = true;
                    }
                }
                else
                {
                    if (!propertyIndexes.Contains(i)) continue;

                    if (isFirstPropertyWritten)
                    {
                        output += ",";
                    }

                    output += GetPropertyValue(properties[i]);
                        
                    if (!isFirstPropertyWritten)
                    {
                        isFirstPropertyWritten = true;
                    }
                }

            }

            return output;
        }

        public virtual void AssignValuesFromCsv(string[] propertyValues)
        {
            var properties = GetType().GetProperties();
            for (var i = 0; i < properties.Length; i++)
            {
                if (properties[i].PropertyType
                    .IsSubclassOf(typeof(CsvableBase)))
                {
                    var instance = Activator.CreateInstance(properties[i].PropertyType);
                    var instanceProperties = instance?.GetType().GetProperties();
                    var propertyList = new List<string>();

                    for (var j = 0; j < instanceProperties?.Length; j++)
                    {
                        propertyList.Add(propertyValues[i + j]);
                    }
                    var m = instance?.GetType().GetMethod("AssignValuesFromCsv", new Type[] { typeof(string[]) });
                    m.Invoke(instance, new object[] { propertyList.ToArray() });
                    properties[i].SetValue(this, instance);

                    i += instanceProperties.Length;
                }
                else
                {
                    var type = properties[i].PropertyType.Name;
                    switch (type)
                    {
                        case "Int32":
                            properties[i].SetValue(this,
                                int.Parse(propertyValues[i]));
                            break;
                        default:
                            properties[i].SetValue(this, propertyValues[i]);
                            break;
                    }
                }
            }
        }

        private string GetPropertyValue(PropertyInfo property)
        {
            var result = string.Empty;
            if (property.PropertyType
                .IsSubclassOf(typeof(CsvableBase)))
            {
                var m = property.PropertyType
                    .GetMethod("ToCsv", Type.EmptyTypes);
                result += m?.Invoke(property.GetValue(this),
                    Array.Empty<object>());
            }
            else
            {
                result = PreProcess(property?.GetValue(this)?.ToString());
            }

            return result;

        }
        private static string PreProcess(string input)
        {
            input = input.Replace("\"", @"""").Trim();
            if (input.Contains(", "))
            {
                input = "\"" + input + "\"";
            }
            return input;
        }
        
    }
}
