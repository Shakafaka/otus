﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;

namespace Otus.HW2
{
    public class CsvReader<T> where T : CsvableBase, new()
    {
        public IEnumerable<T> Read(string filePath, bool hasHeaders)
        {
            var objects = new List<T>();
            using var sr = new StreamReader(filePath);
            var headersRead = false;
            string line;
            do
            {
                line = sr.ReadLine();

                if (line != null && headersRead)
                {
                    var obj = new T();
                    var propertyValues = line.Split(',');
                    obj.AssignValuesFromCsv(propertyValues);
                    objects.Add(obj);
                }
                if (!headersRead)
                {
                    headersRead = true;
                }
            } while (line != null);

            return objects;
        }

        public IEnumerable<T> ReadWithCsvReader(string filePath)
        {
            using var streamReader = File.OpenText(filePath);
            using var csvReader = new CsvReader(streamReader, CultureInfo.CurrentCulture);
            return csvReader.GetRecords<T>();
        }

    }
}
