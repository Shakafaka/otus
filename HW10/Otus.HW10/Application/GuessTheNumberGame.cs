﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.HW10.Interfaces;

namespace Otus.HW10.Application
{
    internal class GuessTheNumberGame : Game
    {
        public GuessTheNumberGame(IRandomGenerator randomGenerator, IConsoleGameInterfaces gameInterface) :
            base(randomGenerator, gameInterface)
        { }

        public override void Play()
        {
            var currentAttempt = 1;
            while (!IsTheGameOver(currentAttempt))
            {
                ShowMessage($"Попытка {currentAttempt} из {_countRetries}");
                CheckTheGuess(_gameInterface.GetInputValue());
                currentAttempt++;
            }
        }

        private void CheckTheGuess(int userNumber)
        {
            if (userNumber == _hiddenNumber)
            {
                _isWon = true;
            }
            else
            {
                if (userNumber < _hiddenNumber) { ShowMessage("Больше!"); }
                if (userNumber > _hiddenNumber) { ShowMessage("Меньше!"); }
            }
        }

        internal override bool IsTheGameOver(int currentAttempt)
        {
            if (!_isWon && currentAttempt <= _countRetries) { return false; }
            ShowEndOfGameMessage();
            return true;

        }
    }
}
