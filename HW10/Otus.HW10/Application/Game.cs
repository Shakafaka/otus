﻿using System.Configuration;
using Otus.HW10.Infrastructure;
using Otus.HW10.Interfaces;

namespace Otus.HW10.Application
{
    public abstract class Game : IGame
    {
        protected int _hiddenNumber;
        protected int _countRetries;
        protected bool _isWon;
        protected IConsoleGameInterfaces _gameInterface;

        public Game(
            IRandomGenerator randomGenerator,
            IConsoleGameInterfaces gameInterface)
        {
            _hiddenNumber = randomGenerator.GetNumber();
            _countRetries = GameConfig.CountRetires;
            _gameInterface = gameInterface;
            _isWon = false;
        }

        internal void ShowEndOfGameMessage()
        {
            ShowMessage(_isWon ? "Поздравляю! Вы угадали!!" : $"Загаданное число: {_hiddenNumber}");
        }

        internal void ShowMessage(string message)
        {
            _gameInterface.SendMessage(message);
        }

        internal virtual bool IsTheGameOver(int currentAttempt)
        {
            return !_isWon;
        }

        public abstract void Play();
    }
}
