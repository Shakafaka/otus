﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.HW10.Application;

namespace Otus.HW10.Infrastructure
{
    internal class GameLauncher
    {
        private readonly Game _game;

        public GameLauncher(Game game)
        {
            _game = game;
        }

        internal void Run()
        {
            _game?.Play();
        }
    }
}
