﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.HW10.Interfaces;

namespace Otus.HW10.Infrastructure
{
    public class RandomGenerator : IRandomGenerator
    {
        private readonly int _minValue = GameConfig.MinValue;
        private readonly int _maxValue = GameConfig.MaxValue;

        public RandomGenerator(int? minValue = null, int? maxValue = null)
        {
            _minValue = minValue ?? _minValue;
            _maxValue = maxValue ?? _maxValue;
        }

        public int GetNumber()
        {
            return new Random().Next(_minValue, _maxValue);
        }
    }
}
