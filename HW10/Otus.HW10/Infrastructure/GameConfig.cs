﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Otus.HW10.Infrastructure
{
    public static class GameConfig
    {
        public static int MinValue { get; private set; } 
        public static  int MaxValue { get; set; }
        public static int CountRetires { get; set; }

        public static void Init (IConfigurationRoot configuration)
        {
            MinValue = int.Parse(configuration.GetSection("MinValue").Value);
            MaxValue = int.Parse(configuration.GetSection("MaxValue").Value);
            CountRetires = int.Parse(configuration.GetSection("CountRetires").Value);
        }
    }
}
