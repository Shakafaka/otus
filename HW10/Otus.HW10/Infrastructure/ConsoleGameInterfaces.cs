﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.HW10.Interfaces;

namespace Otus.HW10.Infrastructure
{
    public class ConsoleGameInterfaces : IConsoleGameInterfaces
    {
        public int GetInputValue()
        {
            return int.Parse(Console.ReadLine()!);
        }

        public void SendMessage(string message)
        {
            Console.WriteLine(message);
        }

        
    }
}
