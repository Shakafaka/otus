﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Otus.HW10.Application;
using Otus.HW10.Infrastructure;

namespace Otus.HW10
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            var config = builder.Build();

            GameConfig.Init(config);

            var game = new GuessTheNumberGame(new RandomGenerator(), new ConsoleGameInterfaces());
            var gameLauncher = new GameLauncher(game);

            Console.WriteLine($"Введите число от {GameConfig.MinValue} до {GameConfig.MaxValue}. У Вас есть {GameConfig.CountRetires} попытки");
            gameLauncher.Run();
        }
    }
}
