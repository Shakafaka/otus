﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.HW8.Interfaces;

namespace Otus.HW8.Entities
{
    /// <summary>
    /// Герой дальнего боя 
    /// </summary>
    public class RangeHero : Hero, IMyCloneable<RangeHero>
    {
        /// <summary>
        /// Дополнительный урон дальнего боя 
        /// </summary>
        public int AdditionalRangeDamage { get; set; }

        public RangeHero(string name, int health, int defense, int meleeDamage, int rangeDamage, int additionalRangeDamage) : base(name, health, defense, meleeDamage, rangeDamage)
        {
            AdditionalRangeDamage = additionalRangeDamage;
        }

        public RangeHero(RangeHero hero) : base(hero)
        {
            AdditionalRangeDamage = hero.AdditionalRangeDamage;
        }

        /// <summary>
        /// Дальняя атака 
        /// </summary>
        /// <returns></returns>
        public override int RangeAttack()
        {
            return RangeDamage + AdditionalRangeDamage;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }
        public override RangeHero MyClone()
        {
            return new RangeHero(this);
        }
    }
}
