﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.HW8.Interfaces;

namespace Otus.HW8.Entities
{
    /// <summary>
    /// Эльфийский герой дальнего боя 
    /// </summary>
    public class ElfRangeHero : RangeHero, IMyCloneable<ElfRangeHero>, ICloneable
    {

        /// <summary>
        /// Дополнительный урон длаьней атаки
        /// </summary>
        private const int AdditionalElfRangeDamage = 10;
        
        public ElfRangeHero(string name, int health, int defense, int meleeDamage, int rangeDamage, int additionalRangeDamage) : base(name, health, defense, meleeDamage, rangeDamage, additionalRangeDamage)
        {
        }

        public ElfRangeHero(ElfRangeHero hero) : base(hero)
        {
        }

        public override int RangeAttack()
        {
            return RangeDamage + AdditionalRangeDamage + AdditionalElfRangeDamage;
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        public override ElfRangeHero MyClone()
        {
            return new ElfRangeHero(this);
        }
    }
}
