﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.HW8.Interfaces;

namespace Otus.HW8.Entities
{
    public class MeleeHero : Hero, IMyCloneable<MeleeHero>

    {
        /// <summary>
        /// Базовый билжний урон
        /// </summary>
        public int AdditionalMeleeDamage { get; set; }

        public MeleeHero(string name, int health, int defense, int meleeDamage, int rangeDamage, int additionalRangeDamage) : base(name, health, defense, meleeDamage, rangeDamage)
        {

        }

        public MeleeHero(Hero hero) : base(hero)
        {
        }

        public override  MeleeHero MyClone()
        {
            return new MeleeHero(this);
        }
    }
}
