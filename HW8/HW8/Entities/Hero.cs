﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus.HW8.Interfaces;

namespace Otus.HW8.Entities
{
    /// <summary>
    /// Герой компьютерной игры 
    /// </summary>
    public class Hero : IMyCloneable<Hero>, ICloneable
    {
        public Hero(string name, int health, int defense, int meleeDamage, int rangeDamage)
        {
            Name = name;
            Health = health;
            Defense = defense;
            MeleeDamage = meleeDamage;
            RangeDamage = rangeDamage;
        }

        public Hero(Hero hero)
        {
            Name = hero.Name;
            Health = hero.Health;
            Defense = hero.Defense;
            MeleeDamage = hero.MeleeDamage;
            RangeDamage = hero.RangeDamage;
        }

        /// <summary>
        /// Базовое Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Базовое здоровье
        /// </summary>
        public int Health { get; set; }

        /// <summary>
        /// Базовоя защита
        /// </summary>
        public int Defense { get; set; }

        /// <summary>
        /// Базовый билжний урон
        /// </summary>
        public int MeleeDamage { get; set; }

        /// <summary>
        /// Базовый дальний урон
        /// </summary>
        public int RangeDamage { get; set; }

        /// <summary>
        /// Базовая билжная атака
        /// </summary>
        public virtual int MeleeAttack()
        {
            return MeleeDamage;
        }

        /// <summary>
        /// Базовая дальняя атака
        /// </summary>
        public virtual int RangeAttack()
        {
            return RangeDamage;
        }


        public object Clone()
        {
            return MemberwiseClone();
        }

        public virtual Hero MyClone()
        {
            return new Hero(this);
        }

    }
}
