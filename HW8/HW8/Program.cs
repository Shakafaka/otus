﻿using System;
using Otus.HW8.Entities;

namespace Otus.HW8
{
    class Program
    {
        static void Main(string[] args)
        {
            var elfRangeHero = new ElfRangeHero(
                name: "Vsevolod",
                health: 100, 
                defense: 50, 
                meleeDamage: 10,
                rangeDamage: 6, 
                additionalRangeDamage: 10);

           var elfRangeHero2 = elfRangeHero.MyClone();
            elfRangeHero2.Name = "Vsevolod2";
          
            var elfRangeHero3 = (ElfRangeHero)elfRangeHero.Clone();
           elfRangeHero3.Name = "Vsevolod3";



            Console.WriteLine(elfRangeHero.Name);
            Console.WriteLine(elfRangeHero2.Name);
            Console.WriteLine(elfRangeHero3.Name);
        }
    }
}
