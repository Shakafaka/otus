﻿using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Otus.HW3.Data;

namespace Otus.HW3
{
    internal class Program
    {
        private const string DateFormat = "dd.MM.yyyy";
        private readonly OtusRepository _repository;

        public Program(OtusRepository repository)
        {
            _repository = repository;
        }

        private static async Task Main()
        {
            var connectionString = GetConnectionString();
            var repository = new OtusRepository(connectionString);
            var program = new Program(repository);

            Console.WriteLine("Выберите действие:\n1. Показать данные\n2. добавить данные");
            var key = Console.ReadKey();
            switch (key.KeyChar)
            {
                case '1':
                    Console.WriteLine("\n");
                    await program.ShowData();
                    return;
                case '2':
                    Console.WriteLine("\nВыберете таблицу для добавления:\n1. Course\n2. Lecture \n3. Student");
                    key = Console.ReadKey();
                    switch (key.KeyChar)
                    {
                        case '1':
                        {
                            await program.ReadCourse();
                        }
                            break;
                        case '2':
                        {
                            await program.ReadLecture();
                        }
                            break;
                        case '3':
                        {
                            await program.ReadStudent();
                        }
                            break;
                        default:
                        {
                            Console.WriteLine("Только 1 или 2");
                        }
                            break;
                    }

                    break;
            }
        }

        private static string GetConnectionString()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false);

            var config = builder.Build();
            return config.GetConnectionString("Default");
        }

        private async Task ReadCourse()
        {
            Console.WriteLine("\n=========Добавление нового курса=========");
            Console.WriteLine("\nИмя: ");
            var courseName = Console.ReadLine();
            Console.WriteLine($"Дата начала: ({DateFormat}):");
            var startDateTime = ReadDateTime();
            Console.WriteLine($"Дата окончания:({DateFormat}):");
            var endDateTime = ReadDateTime();

            var result = await _repository.AddCourse(courseName, startDateTime, endDateTime);
            Console.WriteLine($"Курс добавелн: {result}");
        }

        private async Task ReadLecture()
        {
            Console.WriteLine("\n=========Добавление новой лекции=========");
           
            Console.WriteLine("\nИмя: ");
            var name = Console.ReadLine();
           
            Console.WriteLine("\nОписание: ");
            var description = Console.ReadLine();
          
            Console.WriteLine($"\nДата: ({DateFormat}):");
            var dateTime = ReadDateTime();
           
            Console.WriteLine("\nИдентификатор курса: ");
            var courseIdStr = Console.ReadLine();
           
            if (int.TryParse(courseIdStr, out var courseId) == false)
            {
                Console.WriteLine("Неправильный формат идентификатора курса");
                return;
            }

            var result = await _repository.AddLecture(name, courseId, dateTime, description);
            Console.WriteLine($"Лекция добавлена: {result}");
        }

        private async Task ReadStudent()
        {
            Console.WriteLine("\n=========Добавление новго студента=========");

            Console.WriteLine("\nИмя: ");
            var firstName = Console.ReadLine();

            Console.WriteLine("\nФамилия: ");
            var lastName = Console.ReadLine();

            var result = await _repository.AddStudent(firstName, lastName);
            Console.WriteLine($"Студент добавлен: {result}");
        }

        private DateTime ReadDateTime()
        {
            var startDateStr = Console.ReadLine();
            if (DateTime.TryParseExact(startDateStr, DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out var dateTime) == false)
            {
                Console.WriteLine("Неверный формат даты");
            }

            return dateTime;
        }

        private async Task ShowData()
        {
            var courses = await _repository.GetCourses();
            Console.WriteLine("=========Курсы=========\n");
            foreach (var course in courses)
            {
                Console.WriteLine(course);
            }
            Console.WriteLine("=========Лекции=========\n");
            var lectures = await _repository.GetLectures();
            foreach (var lecture in lectures)
            {
                Console.WriteLine(lecture);
            }

            Console.WriteLine("=========Студенты=========\n");
            var students = await _repository.GetStudents();
            foreach (var student in students)
            {
                Console.WriteLine(student);
            }
        }
    }
}
