﻿INSERT INTO public.courses(name, "start", "end")
VALUES ('Kotlin Developer. Basic', '2021-01-1', '2021-11-1'),
('PHP Developer. Basic', '2021-01-1', '2021-11-1'),
('PHP Developer. Professional', '2021-01-1', '2021-11-1'),
('C# Developer. Professional', '2021-01-1', '2021-11-1'),
('Symfony Framework', '2021-01-1', '2021-11-1');

INSERT INTO public.lectures(
    name, course_id, description, date)
VALUES ('Введение Kotlin', 1, 'Полезное описание', '2021-01-1'),
('Введение в PHP',2,'Полезное описание' , '2021-01-1'),
('Введение в PHP',3,'Полезное описание', '2021-01-1'),
('Введение в C#', 4, 'Полезное описание', '2021-01-1'),
('Введение в Symfony Solution Architecture', 5, 'Полезное описание', '2021-01-1');

INSERT INTO public.students(
    first_name, last_name, registration_date)
VALUES ('Глеб', 'Романов', '2021-01-01'),
('Арина', 'Уткина', '2021-01-01'),
('Кирилл', 'Смирнов', '2021-01-01'),
('Алиса', 'Яковлева', '2021-01-01'),
('Анна', 'Александрова', '2021-01-01');

INSERT INTO public.students_courses(
    student_id, course_id)
VALUES (1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5);