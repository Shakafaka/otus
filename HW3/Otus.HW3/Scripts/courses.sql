﻿CREATE TABLE IF NOT EXISTS public.courses
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    "name" character varying(255) COLLATE pg_catalog."default" NOT NULL,
    "start" date NOT NULL,
    "end" date NOT NULL,
    CONSTRAINT courses_pk PRIMARY KEY (id)
    )


