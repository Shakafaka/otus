﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.HW3.Data.Entities;

namespace Otus.HW3.Data
{
    class OtusRepository
    {
        private readonly string _connectionString;

        public OtusRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<IEnumerable<CourseEntity>> GetCourses()
        {
            var context = new OtusDbContext(_connectionString);
            return await context.Courses
                .Include(x => x.Students)
                .Include(x => x.Lectures)
                .ToListAsync();
        }

        public async Task<IEnumerable<LectureEntity>> GetLectures()
        {
            await using var context = new OtusDbContext(_connectionString);
            return await context.Lectures
                .Include(x => x.CourseEntity)
                .OrderBy(x => x.Id)
                .ToListAsync();
        }

        public async Task<IEnumerable<StudentEntity>> GetStudents()
        {
            await using var context = new OtusDbContext(_connectionString);
            return await context.Students.Include(x => x.Courses)
                .ThenInclude(x => x.CourseEntity)
                .OrderBy(x => x.Id)
                .ToListAsync();
        }

        public async Task<CourseEntity> AddCourse(string name, DateTime startDate, DateTime endDate)
        {
            await using var context = new OtusDbContext(_connectionString);
         
            var course = new CourseEntity(name, startDate, endDate);
            context.Courses.Add(course);
            
            await context.SaveChangesAsync();
            return course;
        }

        public async Task<LectureEntity> AddLecture(string name, int courseId, DateTime date, string description)
        {
            await using var context = new OtusDbContext(_connectionString);

            var lecture = new LectureEntity(name, courseId, description, date);
            context.Lectures.Add(lecture);

            await context.SaveChangesAsync();
            return lecture;
        }

        public async Task<StudentEntity> AddStudent(string firstName, string lastName)
        {
            await using var context = new OtusDbContext(_connectionString);

            var student = new StudentEntity(firstName, lastName);
            context.Students.Add(student);

            await context.SaveChangesAsync();
            return student;
        }

    }
}
