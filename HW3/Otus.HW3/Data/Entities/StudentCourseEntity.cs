﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.HW3.Data.Entities
{
    [Table(("students_courses"))]
    public class StudentCourseEntity
    {
        [Column("student_id")]
        public int StudentId { get; set; }

        [Column("course_id")]
        public int CourseId { get; set; }

        public virtual StudentEntity StudentEntity { get; set; }

        public virtual CourseEntity CourseEntity { get; set; }

    }
}