﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.HW3.Data.Entities
{
    [Table("lectures")]
    public class LectureEntity
    {
        public LectureEntity(string name, int courseId, string description, DateTime date)
        {
            Name = name;
            CourseId = courseId;
            Description = description;
            Date = date;
        }

        [Key, Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("course_id")]
        public int CourseId { get; set; }


        [Column("description")]
        public string Description { get; set; }

        [Column("date")]
        public DateTime Date { get; set; }

        public virtual CourseEntity CourseEntity { get; set; }

        public override string ToString()
        {
            return $"Лекция{Name}. Описание {Description}. Дата {Date:d}";
        }

    }
}
