﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.HW3.Data.Entities
{
    [Table("courses")]
    public class CourseEntity
    {
        [Key, Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("start")]
        public DateTime Start { get; set; }

        [Column("end")]
        public DateTime End { get; set; }

        public virtual ICollection<LectureEntity> Lectures { get; set; }

        public virtual ICollection<StudentCourseEntity> Students { get; set; }

        public CourseEntity(string name, DateTime start, DateTime end)
        {
            Name = name;
            Start = start;
            End = end;
        }
        public override string ToString()
        {
            return $"Курс: {Name} продолжительность: c {Start:d} по {End:d}";
        }

    }
}
