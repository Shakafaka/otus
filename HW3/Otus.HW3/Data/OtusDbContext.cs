﻿using Microsoft.EntityFrameworkCore;
using Otus.HW3.Data.Entities;

namespace Otus.HW3.Data
{
    class OtusDbContext : DbContext
    {
        private readonly string _connectionString;

        public OtusDbContext(string connectionString)
        {
            _connectionString = connectionString;
        }

        public DbSet<CourseEntity> Courses { get; set; }

        public DbSet<LectureEntity> Lectures { get; set; }

        public DbSet<StudentEntity> Students { get; set; }

        public DbSet<StudentCourseEntity> StudentCourses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(_connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LectureEntity>()
                .HasOne<CourseEntity>(x => x.CourseEntity)
                .WithMany(x => x.Lectures)
                .HasForeignKey(x => x.CourseId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<StudentCourseEntity>(e =>
            {
                e.HasKey(x => new { x.StudentId, x.CourseId });

                e.HasOne(x => x.CourseEntity)
                    .WithMany(x => x.Students)
                    .HasForeignKey(x => x.CourseId)
                    .OnDelete(DeleteBehavior.Cascade);

                e.HasOne(x => x.StudentEntity)
                    .WithMany(x => x.Courses)
                    .HasForeignKey(x => x.StudentId)
                    .OnDelete(DeleteBehavior.Cascade);
            });
        }

    }
}