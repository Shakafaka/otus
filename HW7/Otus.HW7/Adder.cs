﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChoETL;

namespace Otus.HW7
{
    public class Adder
    {
        private readonly int[] _array;

        public Adder(int length)
        {
            var random = new Random();
            _array = new int[length];
            for (var i = 0; i < length; i++)
            {
                _array[i] = random.Next(-100, 100);
            }
        }

        public long GetDefaultSumTime()
        {
            Stopwatch stopwatch = new();
            stopwatch.Start();
            _array.Sum();
            stopwatch.Stop();
            return stopwatch.ElapsedMilliseconds;
        }

        public long GetThreadSumTime()
        {
            Stopwatch stopwatch = new();
            stopwatch.Start();
            ThreadSum(SplitChunks(), new Task<int>[4]);
            stopwatch.Stop();
            return stopwatch.ElapsedMilliseconds;
        }

        private IEnumerable<int[]> SplitChunks()
        {
            return _array.Chunk(_array.Length / 4).Select(t=>t.ToArray()); 
        }

        public long GetPLinqSumTime()
        {
            Stopwatch stopwatch = new();
            stopwatch.Reset();
            stopwatch.Start(); _array.AsParallel().Sum();
            stopwatch.Stop();
            return stopwatch.ElapsedMilliseconds;
        }

        private static int ThreadSum(IEnumerable<int[]> chunks, Task<int>[] taskArray)
        {
            var i = 0;
            foreach (var chunk in chunks)
            {
                taskArray[i] = Task.Run(() => chunk.Sum());
                i++;
            }
            Task.WaitAll(taskArray);
            return taskArray.Sum(t => t.Result);
        }
    }
}
