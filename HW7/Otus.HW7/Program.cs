﻿using System;

namespace Otus.HW7
{
    class Program
    {
        static void Main(string[] args)
        {
            var count = 100000;
            var adder = new Adder(count);
            Console.WriteLine($"Колличество элементов: {count}");
            Console.WriteLine($"Обычный метод: {adder.GetDefaultSumTime()}");
            Console.WriteLine($"Thread метод: {adder.GetThreadSumTime()}");
            Console.WriteLine($"PLinq метод: {adder.GetPLinqSumTime()}");
            
            count = 1000000; 
            adder = new Adder(count);

            Console.WriteLine($"Колличество элементов: {count}");
            Console.WriteLine($"Обычный метод: {adder.GetDefaultSumTime()}");
            Console.WriteLine($"Thread метод: {adder.GetThreadSumTime()}");
            Console.WriteLine($"PLinq метод: {adder.GetPLinqSumTime()}");

            count = 10000000;
            adder = new Adder(count);
            Console.WriteLine($"Колличество элементов: {count}");
            Console.WriteLine($"Обычный метод: {adder.GetDefaultSumTime()}");
            Console.WriteLine($"Thread метод: {adder.GetThreadSumTime()}");
            Console.WriteLine($"PLinq метод: {adder.GetPLinqSumTime()}");

            Console.ReadLine();
        }
    }
}
