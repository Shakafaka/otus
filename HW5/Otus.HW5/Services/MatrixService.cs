﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Extensions.Logging;
using Otus.HW5.DTO;
using Otus.HW5.Enums;
using Otus.HW5.Extensions;
using Otus.HW5.Interfaces;
using Otus.HW5.Strategies.CalculateMatricesStrategy.CalculateMatricesStrategyActions;
using Otus.HW5.Strategies.CalculateMatricesStrategy.Interfaces;
using Superpower;
using Superpower.Model;
using Superpower.Parsers;

namespace Otus.HW5.Services
{
    public class MatrixService : IMatrixService
    {
        private readonly ICalculateMatricesStrategyContext _context;
        private readonly IFileService _fileService;
        public MatrixService(ICalculateMatricesStrategyContext context, IFileService fileService)
        {
            _context = context;
            _fileService = fileService;
        }

        /// <summary>
        /// Выполнить операции над матрицами
        /// </summary>
        public void CalculateMatrices(string filePath)
        {
            var files = Directory.GetFiles(filePath);

            foreach (var file in files.AsParallel())
            {
                if (!_fileService.IsFileExists(file))
                {
                    continue;
                }
          

                var fileData = File.ReadAllText(file, Encoding.UTF8);
                var parserResult = GetMatricesFromFileData(fileData);

                if (!parserResult.HasValue)
                {
                    throw new Exception($"Файл: {file}. Неверный синтаксис. {parserResult.ToString()} {Environment.NewLine}");
                    continue;
                }

                try
                {
                    var resultCaluleate = Calculate(parserResult);
                    if (string.IsNullOrEmpty(resultCaluleate))
                    {
                        continue;
                    }
                    
                    _fileService.SaveFileWithText(file, resultCaluleate);

                }
                catch (Exception e)
                {
                    throw new Exception($"Файл {file} вызвал ошибку: {e.Message} {Environment.NewLine}");
                }
            }
        }
        /// <summary>
        /// Получить матрицы из данных файла 
        /// Используется Superpower (https://github.com/datalust/superpower)
        /// </summary>
        private Result<MatricesDTO> GetMatricesFromFileData(string fileData)
        {

            var whitespace = Character.EqualTo(' ');
            var newLine = Span.EqualTo("\n").Or(Span.EqualTo("\r\n"));
            var blankLines = newLine.Try().AtLeastOnceDelimitedBy(whitespace.Many());

            var row = Numerics.DecimalDouble.Try().AtLeastOnceDelimitedBy(whitespace.AtLeastOnce())
                .Between(whitespace.Many(), whitespace.Many());
            var matrix = row.AtLeastOnceDelimitedBy(newLine);
            var manyMatrices = matrix.AtLeastOnceDelimitedBy(blankLines);

            var commandName = Character.Letter.AtLeastOnce()
                .Between(whitespace.Many(), whitespace.Many())
                .Select(_ => new string(_));

            var expression = from leading in Character.WhiteSpace.Many()
                             from command in commandName
                             from lines in blankLines
                             from matrices in manyMatrices
                             from trailing in Character.WhiteSpace.Many().AtEnd()
                             select new MatricesDTO(command, matrices);

            return expression.TryParse(fileData);
        }

        /// <summary>
        /// Вычислить матрицы 
        /// </summary>
        private string Calculate(Result<MatricesDTO> request)
        {
            var matrixAction = request.Value.command.ToEnum(MatrixActions.NotFound);
            var matrices = request.Value.matrices;
            switch (matrixAction)
            {
                case MatrixActions.Multiply:
                    {
                        _context.SetStrategy(new CalculateMatricesStrategyMultiply());
                        return _context.ExecuteStrategy(matrices);
                    }
                case MatrixActions.Add:
                    {
                    
                        _context.SetStrategy(new CalculateMatricesStrategyAdd());
                        return _context.ExecuteStrategy(matrices);

                    }
                case MatrixActions.Subtract:
                    {
                
                        _context.SetStrategy(new CalculateMatricesStrategySubtract());
                        return _context.ExecuteStrategy(matrices);

                    }
                case MatrixActions.Transpose:
                    {
                        _context.SetStrategy(new CalculateMatricesStrategyTranspose());
                        return _context.ExecuteStrategy(matrices);

                    }
                case MatrixActions.NotFound:
                default:
                   
                    return string.Empty;
            }
        }

        /// <summary>
        /// Проверка на минимальное количество матриц
        /// </summary>
        private bool CheckMatrixLength(double[][][] matrices)
        {
            //NEW FEATURE
            var isInvalidLength = matrices?.Length is < 2;
            if (isInvalidLength)
            {
                throw new Exception($"Недостаточное количество матриц для операции {Environment.NewLine}");
            }
            return isInvalidLength;
        }
    }
}
