﻿using System.IO;
using Otus.HW5.Interfaces;

namespace Otus.HW5.Services
{
    public class FileService : IFileService
    {
        /// <summary>
        /// Проверка на существование файла 
        /// </summary>
        public bool IsFileExists(string filePath)
        {
            return File.Exists(filePath);
        }
        /// <summary>
        /// Сохранить или обновить файл 
        /// </summary>
        public void SaveFileWithText(string filePath, string text)
        {
            var newFilePath = GetNewFilePath(filePath);

            CreateDirectoryIfNotExists(newFilePath);

            DeleteFileIfExists(newFilePath);

            using StreamWriter sw = File.CreateText(newFilePath);
            sw.Write(text);
        }
        private string GetNewFilePath(string filePath)
        {
            var newFileName = Path.GetFileName(filePath).Replace(Path.GetFileNameWithoutExtension(filePath), Path.GetFileNameWithoutExtension(filePath) + "_result");
            var newFilePath = Path.Combine(Path.GetDirectoryName(filePath), "Results", newFileName);
            return newFilePath;
        }
        private void DeleteFileIfExists(string filePath)
        {
            if (IsFileExists(filePath))
            {
                File.Delete(filePath);
            }
        }
        private void CreateDirectoryIfNotExists(string filePath)
        {
            var directory = Path.GetDirectoryName(filePath);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }
    }
}
