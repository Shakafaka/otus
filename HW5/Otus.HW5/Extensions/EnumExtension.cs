﻿using System;

namespace Otus.HW5.Extensions
{
    public static class EnumExtension
    {
        public static TEnum ToEnum<TEnum>(this string strEnumValue, TEnum defaultValue)
        {
            if (!Enum.IsDefined(typeof(TEnum), strEnumValue.ToLower()))
                return defaultValue;

            return (TEnum)Enum.Parse(typeof(TEnum), strEnumValue.ToLower());
        }
    }
}
