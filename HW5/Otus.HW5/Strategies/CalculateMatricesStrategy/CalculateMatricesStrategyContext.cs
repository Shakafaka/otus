﻿using Otus.HW5.Strategies.CalculateMatricesStrategy.Interfaces;

namespace Otus.HW5.Strategies.CalculateMatricesStrategy
{
    public class CalculateMatricesStrategyContext : ICalculateMatricesStrategyContext
    {
        private ICalculateMatricesStrategy _strategy;

        public void SetStrategy(ICalculateMatricesStrategy strategy)
        {
            _strategy = strategy;
        }
        public string ExecuteStrategy(double [][][] matrices)
        {
           return _strategy.Calculate(matrices);
        }

    }
}
