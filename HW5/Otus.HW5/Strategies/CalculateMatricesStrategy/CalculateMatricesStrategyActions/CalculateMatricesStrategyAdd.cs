﻿using MathNet.Numerics.LinearAlgebra;
using Otus.HW5.Strategies.CalculateMatricesStrategy.Interfaces;

namespace Otus.HW5.Strategies.CalculateMatricesStrategy.CalculateMatricesStrategyActions
{
    class CalculateMatricesStrategyAdd : ICalculateMatricesStrategy
    {
        public string Calculate(double[][][] matrices)
        {

            var currentMatrix = CreateMatrix.DenseOfRowArrays(matrices[0]);
            for (var i = 1; i < matrices.Length; i++)
            {
                currentMatrix = currentMatrix.Add(CreateMatrix.DenseOfRowArrays(matrices[i]));
            }
            return currentMatrix.ToMatrixString();
        }
    }
}
