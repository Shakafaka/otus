﻿using System;
using System.Linq;
using MathNet.Numerics.LinearAlgebra;
using Otus.HW5.Strategies.CalculateMatricesStrategy.Interfaces;

namespace Otus.HW5.Strategies.CalculateMatricesStrategy.CalculateMatricesStrategyActions
{
    class CalculateMatricesStrategyTranspose : ICalculateMatricesStrategy
    {
        public string Calculate(double[][][] matrices)
        {
            return matrices.Select(t => CreateMatrix.DenseOfRowArrays(t).Transpose())
                .Aggregate(string.Empty, (current, matrix) => string.Concat(current, matrix.ToMatrixString(), Environment.NewLine));
        }
    }
}

