﻿namespace Otus.HW5.Strategies.CalculateMatricesStrategy.Interfaces
{
    public interface ICalculateMatricesStrategyContext
    {
        void SetStrategy(ICalculateMatricesStrategy strategy);
        string ExecuteStrategy(double[][][] matrices);
    }
}
