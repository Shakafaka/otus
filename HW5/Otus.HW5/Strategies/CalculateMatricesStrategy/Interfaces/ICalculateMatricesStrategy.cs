﻿namespace Otus.HW5.Strategies.CalculateMatricesStrategy.Interfaces
{
    public interface ICalculateMatricesStrategy
    {
        string Calculate(double[][][] matrices);
    }
}
