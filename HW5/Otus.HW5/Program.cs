// See https://aka.ms/new-console-template for more information
//NEW FEATURE
using System;
using System.ComponentModel;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Otus.HW5.Interfaces;
using Otus.HW5.Services;
using Otus.HW5.Strategies.CalculateMatricesStrategy;
using Otus.HW5.Strategies.CalculateMatricesStrategy.Interfaces;

//NEW FEATURE
var serviceProvider = new ServiceCollection()
    .AddLogging()
    .AddTransient<IMatrixService, MatrixService>()
    .AddTransient<ICalculateMatricesStrategyContext, CalculateMatricesStrategyContext>()
    .AddTransient<IFileService, FileService>()
    .BuildServiceProvider();

serviceProvider
    .GetService<ILoggerFactory>()
    .AddConsole();

var matrixService = serviceProvider.GetService<IMatrixService>();
//NEW FEATURE
FileService fileService = new();
//NEW FEATURE
if (fileService is null) { Console.WriteLine("������ ������ �� ���������������"); }
while (true)
{
    Console.WriteLine("��� ������ ������� exit");
    Console.WriteLine("������� ���� �� ����� � ���������:");
    var filePath = Console.ReadLine();
    if (filePath.ToLower() == "exit")
    {
        break;
    }
    if (Directory.Exists(filePath))
    {
        matrixService.CalculateMatrices(filePath);
    }
    else
    {
        Console.WriteLine("������� �� ������������ ����������");
    }

}
