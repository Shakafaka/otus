﻿namespace Otus.HW5.Interfaces
{
    public interface IMatrixService
    {
        /// <summary>
        /// Выполнить операции над матрицами
        /// </summary>
        void CalculateMatrices(string filePath);
    }
}
