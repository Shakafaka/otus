﻿namespace Otus.HW5.Interfaces
{
   public interface IFileService
    {
        /// <summary>
        /// Создать или обновить файл 
        /// </summary>
        void SaveFileWithText(string filePath, string text);

        /// <summary>
        ///  Проверка на существование файла 
        /// </summary>
        bool IsFileExists(string filePath);
    }
}
