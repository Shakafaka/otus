﻿namespace Otus.HW5.Enums
{
    public enum MatrixActions
    {
        Multiply,
        Add,
        Subtract,
        Transpose,
        NotFound,
    }
}
