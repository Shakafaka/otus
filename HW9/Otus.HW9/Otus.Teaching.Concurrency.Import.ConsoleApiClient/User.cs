﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.ConsoleApiClient
{
    public class User
    {
        public User(string name)
        {
            Name = name;
        }

        [JsonPropertyName("name")]
        public string Name { get; set; }

    }
}
