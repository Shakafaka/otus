﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;


namespace Otus.Teaching.Concurrency.Import.ConsoleApiClient
{
    class Program
    {
        private static readonly HttpClient _client = new HttpClient();

        static async Task Main(string[] args)
        {
            await AddRandomUser();
            await GetUserById();
            await AddUser();
            await AddRandomUser();
            await GetAllUsers();
        }

        
        private static async Task GetAllUsers()
        {
            Console.WriteLine("Пользователи: ");
            await ProcessCustomers();
        }

        private static async Task AddRandomUser()
        {
            Console.WriteLine("Добавление случайного пользователя: ");
            var randomCustomer = GenerateCustomer();
            var requestRandom = await AddUser(randomCustomer);
            Console.WriteLine(requestRandom.StatusCode == HttpStatusCode.OK
                ? $"Новый пользователь был добавлен"
                : "Bad request");
        }

        private static async Task AddUser()
        {
            Console.WriteLine("Введите имя: ");
            var name = Console.ReadLine();

            var request = await AddUser(name);
            Console.WriteLine(request.StatusCode == HttpStatusCode.OK ? $"Новый пользователь добавлен!" : "Bad request");
        }

        private static async Task GetUserById()
        {
            Console.WriteLine("Введите идентификатор пользователя:");
            if (int.TryParse(Console.ReadLine(), out var id))
            {
                var response = await GetUserResponse(id);
                if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    Console.WriteLine($"Пользователь с идентификатором: {id} не найден");
                }
                else
                {
                    var user = await GetUser(response);
                    Console.WriteLine($"Id: {id}, имя: {user.Name}");
                }
            }
        }

        private static User GenerateCustomer()
        {
            return new User(Faker.Name.FullName());
        }

        private static async Task<HttpResponseMessage> AddUser(string name)
        {
            var user = new User(name);
            var json = JsonSerializer.Serialize<User>(user);
            var content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await _client.PostAsync($"https://localhost:5001/api/user/createuser", content);
            return request;
        }
        private static async Task<HttpResponseMessage> AddUser(User user)
        {
            var json = JsonSerializer.Serialize<User>(user);
            var content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await _client.PostAsync($"https://localhost:5001/api/user/createuser", content);
            return request;
        }

        private static async Task<User> GetUser(HttpResponseMessage responce)
        {
            var json = await responce.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<User>(await responce.Content.ReadAsStringAsync());
        }

        private static async Task<HttpResponseMessage> GetUserResponse(int id)
        {
            return await _client.GetAsync($"https://localhost:5001/api/user/{id}");
        }

        private static async Task ProcessCustomers()
        {
            var streamTask = _client.GetStreamAsync("https://localhost:5001/api/user/GetUsers");
            var users = await JsonSerializer.DeserializeAsync<List<User>>(await streamTask);

            foreach (var user in users)
            {
                Console.WriteLine(user.Name);
            }
        }


    }
}
