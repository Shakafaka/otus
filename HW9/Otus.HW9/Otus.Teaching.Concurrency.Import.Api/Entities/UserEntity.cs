﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Api.Entities
{
    public class UserEntity
    {
        [Key]
        public int? Id { get; set; }
        public string Name { get; set; }

    }
}
