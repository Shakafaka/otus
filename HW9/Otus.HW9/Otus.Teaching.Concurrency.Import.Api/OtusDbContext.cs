﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Api.Entities;

namespace Otus.Teaching.Concurrency.Import.Api
{
    public class OtusDbContext : DbContext
    {
        public OtusDbContext(DbContextOptions<OtusDbContext> options) : base(options)
        {

        }
        public DbSet<UserEntity> Users { get; set; }

    }
}
