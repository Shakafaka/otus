﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Api.Entities;

namespace Otus.Teaching.Concurrency.Import.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly OtusDbContext _context;

        public UserController(OtusDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<UserEntity> GetUsers()
        {
            var customers = _context.Users;

            return customers;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetUserById([FromQuery] int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null) return NotFound(id);

            return Ok(user);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<IActionResult> CreateUser([FromBody] UserEntity user)
        {
            var isUserExist = _context.Users.Any(t => t.Id == user.Id);
            if (isUserExist) { return Conflict(user.Id); }

            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            return Ok(user.Id);
        }

    }
}
