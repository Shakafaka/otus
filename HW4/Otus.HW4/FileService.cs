﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.HW4
{
    internal class FileService
    {
        public event EventHandler<FileArgs>? FileFound;
       
        private readonly string _fileDirectory;
        public FileService(string fileDirectory)
        {
            _fileDirectory =  fileDirectory;
        }
        public IEnumerable<string> GetFiles(CancellationToken? token = null)
        {
            var files = Directory.GetFiles(_fileDirectory);
            foreach (var fileName in files)
            {
                if (token?.IsCancellationRequested ?? false)
                {
                    break;
                }
                FileFound?.Invoke(this, new FileArgs(fileName));
                yield return fileName;
            }
        }
    }
}
