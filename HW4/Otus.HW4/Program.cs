﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Otus.HW4
{
    public static class Program
    {
        public static void Main()
        {
            var fileService = new FileService(Directory.GetCurrentDirectory());

            fileService.FileFound += (sender, eventArgs) => Console.WriteLine(eventArgs.FileName);

            var foundedFiles = new List<string>();

            foreach (var file in fileService.GetFiles(GetCancellationToken()))
            {
                foundedFiles.Add(file);
            }

            var maxFileName = foundedFiles.GetMaxLength((string file) => file.Length);
            if (!string.IsNullOrEmpty(maxFileName)) { Console.WriteLine($"Самое большое имя у файла: {maxFileName}"); }
            Console.ReadLine();
        }

        public static T GetMaxLength<T>(this IEnumerable<T> e, Func<T, float> length) where T : class
        {
            T? response = default;
            float? maxValue = null;

            using var enumerator = e.GetEnumerator();
            while (enumerator.MoveNext())
            {
                var currentLength = length(enumerator.Current);
                if (maxValue == null || currentLength > maxValue)
                {
                    maxValue = currentLength;
                    response = enumerator.Current;
                }

            }
            return response;
        }

        private static CancellationToken GetCancellationToken()
        {
            CancellationTokenSource cancelTokenSource = new();

            Console.WriteLine("Прервать поиск? у/n");

            var isCanceled = Console.ReadLine();
            if (isCanceled?.ToLower() == "y") { cancelTokenSource.Cancel(); }

            return cancelTokenSource.Token;
        }
    }
}

